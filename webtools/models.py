from django.db import models
from django.core import validators
from datetime import datetime,tzinfo,timedelta
from django.contrib.auth.models import User

import time
import re
import os

class UrlQuery(models.Model):
	url = models.CharField(max_length=256) 
	time = models.DateTimeField(auto_now_add=True)
	source = models.CharField(max_length=30)
	user = models.ForeignKey(User)

			
	def __unicode__(self):
		return self.url

	def unicode(self):
		return self.__unicode__()

