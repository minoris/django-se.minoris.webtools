from django.conf.urls import patterns, url
from django.views.generic import DetailView, ListView

from webtools import views
from webtools.models import *

urlpatterns = patterns('', 
	url(r'^$',
		views.main),
)
