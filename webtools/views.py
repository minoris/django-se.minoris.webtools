from django.template import RequestContext, Context, loader
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from webtools.models import *
from django.contrib.auth.decorators import login_required, user_passes_test
from django import template
from django import forms

from decimal import Decimal
import decimal
import os
import random
import string
import urlparse

import pycurl
import sys

register = template.Library()

@register.inclusion_tag("results.html")
def show_results(poll):
	return { 'test', [1,2,3] }

class Storage:
	def __init__(self):
		self.contents = ''
		self.line = 0

	def store(self, buf):
		self.line = self.line + 1
		self.contents += buf

	def __str__(self):
		return self.contents





class HttpHeaderForm(forms.Form):
	host = forms.CharField(label="Host", max_length=256, required=False)
	query = forms.CharField(initial="http://google.se", label='URL', max_length=256, required=True)

@csrf_exempt
def main(request):
	key = ""
	output = ""

	if request.method == 'POST':
		form = HttpHeaderForm(request.POST)
	else:
		form = HttpHeaderForm()

	if ( form.is_valid() ):
		key = str(form.cleaned_data['query'])
		print key

			
		url = urlparse.urlsplit(key)
		hostname = str(url.hostname)

		if ( form.cleaned_data['host'] != '' ):	
			key = urlparse.urlunsplit([url.scheme, str(form.cleaned_data['host']), url.path, url.query, url.fragment])
		else:
			key = urlparse.urlunsplit([url.scheme, url.netloc, url.path, url.query, url.fragment])
	
		try:	
			retrieved_body = Storage()
			retrieved_headers = Storage()
			c = pycurl.Curl()
			c.setopt(c.URL, key)
			c.setopt(c.WRITEFUNCTION, retrieved_body.store)
			c.setopt(c.SSL_VERIFYHOST, 0)
			c.setopt(c.HEADERFUNCTION, retrieved_headers.store)
			c.setopt(c.HTTPHEADER, ['Host:' + hostname])
			c.perform()
			c.close()

			output = str(retrieved_headers)
		except Exception as e:
			output = str(e)
	context = Context({
			'key': key,
			'output': output,
			'form': form,
	})
	return render(request, "webtools/index.html", context)

