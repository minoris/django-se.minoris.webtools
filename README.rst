========
Webtools
========

Webtools is a simple library to do some basic webserver checks

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "webtools" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'webtools',
    )

2. Add the url to the url patterns like this::

    urlpatterns = patterns('', 
        url(r'^webtools/', include('webtools.urls')),
    )

3. Create link to the application for easy access

4. Run `python manage.py migrate` to create the webtools models.

