import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-se.minoris.webtools',
    version='0.1.4',
    packages=['webtools'],
    include_package_data=True,
    license='BSD License',
    description='A simple Django library to check workings of http servers.',
    long_description=README,
    url='https://minoris.se/se.minoris.webtools/',
    author='Kent Gustavsson',
    author_email='kent@minoris.se',
	install_requires=['pycurl'],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)


